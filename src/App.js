import React from 'react';
import './App.css';
import FormContainer from './form/formContainer';
import FormComponent from './form/formComponent';
const Form = FormContainer(FormComponent);
class App extends React.Component {
  render(){
    return (
      <div className="App">
        <header className="App-header">
        <Form/>
        </header>
      </div>
    );
  }
  
}

export default App;
